package controlefornecedor.model;

import java.util.ArrayList;

public class Fornecedor {
    
    private String nome;
    private ArrayList<String> telefones;
    private Endereco endereco;
    private ArrayList<Produto> produtos;
    private boolean juridico;

    public boolean getJuridico(){
        return juridico;
    }
    public void setJuridico(boolean juridico){
        this.juridico=juridico;
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ArrayList<String> getTelefones() {
        return telefones;
    }

    public void setTelefones(ArrayList<String> telefones) {
        this.telefones = telefones;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public ArrayList<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }
    
}
