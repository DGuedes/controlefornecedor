
package controlefornecedor.controller;
import controlefornecedor.model.Produto;

import java.util.ArrayList;

public class ControleProdutos {
    
    private ArrayList listaProdutos = new ArrayList();
    
    public String AdicionarProduto(String umProduto){        
        listaProdutos.add(umProduto);
        return "produto adicionado";
    }
    public ArrayList RetornarLista(){
        return listaProdutos;
    }
    
}
