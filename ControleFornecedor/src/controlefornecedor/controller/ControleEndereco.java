package controlefornecedor.controller;
import controlefornecedor.model.Endereco;

public class ControleEndereco {
    private Endereco umEndereco;
    
    public Endereco RetornarEndereco(){
        return umEndereco;
    }
    public void ArmazenarEndereco(Endereco umEndereco){
        this.umEndereco = umEndereco;
    }
    
}
