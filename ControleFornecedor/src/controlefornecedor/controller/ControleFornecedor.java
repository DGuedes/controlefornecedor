package controlefornecedor.controller;
import controlefornecedor.model.Fornecedor;
import controlefornecedor.model.PessoaFisica;
import java.util.ArrayList;
import controlefornecedor.model.PessoaJuridica;

public class ControleFornecedor {
    private ArrayList<Fornecedor> listaFornecedores = new ArrayList<Fornecedor>();
    VariavelJuridica umaVariavelJuridica = new VariavelJuridica();
    
    public void adicionarPessoaFisica(PessoaFisica fornecedor){
        listaFornecedores.add((Fornecedor)fornecedor);    
    }
    public void removerPessoa(Fornecedor umFornecedor){
        listaFornecedores.remove(umFornecedor);
    }
    
    public String adicionarPessoaJuridica(PessoaJuridica fornecedor){
        listaFornecedores.add(fornecedor);
        return "Pessoa juridica adicionada.";
    }
    
    public void ListarFornecedores(){
        for(Fornecedor umFornecedor: listaFornecedores){
            System.out.println(umFornecedor.getNome());
        }        
    }
    
    public void ListarUmFornecedor(Fornecedor umFornecedor){
        if(!umFornecedor.getNome().equals(false)){
            System.out.println(umFornecedor.getEndereco());
            System.out.println(umFornecedor.getJuridico());
            System.out.println(umFornecedor.getNome());
            System.out.println(umFornecedor.getProdutos());
        
        }
    }
    public Fornecedor PesquisarFornecedor(String umNome){
         for(Fornecedor umFornecedor: listaFornecedores){
            if(umFornecedor.getNome().equalsIgnoreCase(umNome)){
                return umFornecedor;
            }
        }
        return null;
    }
    

    
    
}