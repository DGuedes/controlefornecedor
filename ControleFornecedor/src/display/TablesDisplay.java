package display;
//obs: codigo usado como base: http://www.coderanch.com/t/425544/GUI/java/add-empty-row-Jtable
import javax.swing.*;  
import java.awt.*;  
import java.awt.event.*;  
import javax.swing.table.*;  
  
class TablesDisplay{
    
    int totalCadastros;
    
    public TablesDisplay(int totalCadastros){
        this.totalCadastros = totalCadastros;
    }
    public void insertRow(){
        ((DefaultTableModel)table.getModel()).addRow(new java.util.Vector<String>(java.util.Arrays.asList(new String[]{String.valueOf(x++),"",""})));
    }
        
    
    JTable table;    
    int x = 0;
    
    public void buildGUI(){          
    String teste = "nome";
    DefaultTableModel umDefaultTableModel;    
    umDefaultTableModel = new DefaultTableModel(0,7); 
    table = new JTable(umDefaultTableModel);
    for(int contador = 0; contador < totalCadastros; contador++) insertRow();  
    JScrollPane umScroll = new JScrollPane(table);  
    JFrame umTableFrame = new JFrame();  
    umTableFrame.getContentPane().add(umScroll);   
    umTableFrame.pack();  
    umTableFrame.setLocationRelativeTo(null);  
    umTableFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);  
    umTableFrame.setVisible(true);
    }            
    
}  