package display;

import controlefornecedor.controller.ControleTelefone;
import controlefornecedor.controller.ControleProdutos;
import controlefornecedor.model.PessoaJuridica;
import controlefornecedor.controller.ControleEndereco;
import controlefornecedor.controller.ControleFornecedor;

public class AdicionarFornecedorJuridico extends javax.swing.JFrame {
    
    ControleTelefone umControleTelefone = new ControleTelefone();
    ControleProdutos umControleProdutos = new ControleProdutos();
    PessoaJuridica umaPessoaJuridica = new PessoaJuridica();
    ControleEndereco umControleEndereco = new ControleEndereco();
    ControleFornecedor umControleFornecedor;
    boolean juridico;
    
    public void LimparTelaJuridica(){
    jTextFieldAdicionarCNPJ.setText("");
    jTextFieldAdicionarNome.setText("");
    jTextFieldAdicionarProduto.setText("");
    jTextFieldAdicionarTelefone.setText("");
    jTextFieldRazao.setText("");
    }

    /** Creates new form AdicionarFornecedorJuridico */
    public AdicionarFornecedorJuridico(ControleFornecedor umControleFornecedor, boolean juridico) {
        initComponents();
        LimparTelaJuridica();
        this.juridico = juridico;
        this.umControleFornecedor = umControleFornecedor;    
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jTextFieldAdicionarNome = new javax.swing.JTextField();
        jTextFieldAdicionarProduto = new javax.swing.JTextField();
        jTextFieldAdicionarCNPJ = new javax.swing.JTextField();
        jTextFieldRazao = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jTextFieldAdicionarTelefone = new javax.swing.JTextField();
        jButtonAdicionarProduto = new javax.swing.JButton();

        jLabel1.setText("Nome:");

        jLabel2.setText("Endereço:");

        jLabel3.setText("Telefones:");

        jLabel4.setText("Produtos:");

        jLabel5.setText("CNPJ:");

        jLabel6.setText("Razão Social:");

        jButton1.setText("Adicionar Endereço");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Adicionar Telefone");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Salvar Pessoa Jurídica");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButtonAdicionarProduto.setText("Adicionar Produto");
        jButtonAdicionarProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdicionarProdutoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(53, 53, 53)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel5)
                    .addComponent(jLabel4)
                    .addComponent(jLabel3)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton3)
                    .addComponent(jButton1)
                    .addComponent(jTextFieldAdicionarNome, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jTextFieldRazao, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldAdicionarCNPJ, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldAdicionarProduto, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldAdicionarTelefone, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jButtonAdicionarProduto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(102, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextFieldAdicionarNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jButton1))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextFieldAdicionarTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton2)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jTextFieldAdicionarProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonAdicionarProduto))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jTextFieldAdicionarCNPJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jTextFieldRazao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jButton3)
                .addContainerGap(42, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
    EnderecoAdicionar frame = new EnderecoAdicionar(umControleEndereco);     
    frame.setVisible(true);      
}//GEN-LAST:event_jButton1ActionPerformed

private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
    umControleTelefone.AdicionarTelefone(jTextFieldAdicionarTelefone.getText());
    jTextFieldAdicionarTelefone.setText("");
}//GEN-LAST:event_jButton2ActionPerformed

private void jButtonAdicionarProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarProdutoActionPerformed
    umControleProdutos.AdicionarProduto(jTextFieldAdicionarProduto.getText());
    jTextFieldAdicionarProduto.setText("");
    
}//GEN-LAST:event_jButtonAdicionarProdutoActionPerformed

private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
    umaPessoaJuridica.setNome(jTextFieldAdicionarNome.getText());
    umaPessoaJuridica.setCnpj(jTextFieldAdicionarCNPJ.getText());
    umaPessoaJuridica.setEndereco(umControleEndereco.RetornarEndereco());
    umaPessoaJuridica.setTelefones(umControleTelefone.RetornarLista());
    umaPessoaJuridica.setProdutos(umControleProdutos.RetornarLista());
    umaPessoaJuridica.setJuridico(juridico);
    umControleFornecedor.adicionarPessoaJuridica(umaPessoaJuridica);
    System.out.println("Pessoa Fisica Adicionada!");
    setVisible(false);
    dispose();
}//GEN-LAST:event_jButton3ActionPerformed

    /**
     * @param args the command line arguments
     */
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButtonAdicionarProduto;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JTextField jTextFieldAdicionarCNPJ;
    private javax.swing.JTextField jTextFieldAdicionarNome;
    private javax.swing.JTextField jTextFieldAdicionarProduto;
    private javax.swing.JTextField jTextFieldAdicionarTelefone;
    private javax.swing.JTextField jTextFieldRazao;
    // End of variables declaration//GEN-END:variables
}
