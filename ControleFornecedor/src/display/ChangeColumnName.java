 import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;

public class ChangeColumnName{
  JTable table;
  public static void main(String arg[]){
    new ChangeColumnName();
  }

  public ChangeColumnName(){
    JFrame frame = new JFrame("Changing Column Name!");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    JPanel panel = new JPanel();
    String data[][] = {{"Vinod","MCA","Computer"},
                       {"Deepak","PGDCA","History"},
                       {"Ranjan","M.SC.","Biology"},
                       {"Radha","BCA","Computer"}};
    String col[] = {"Name","Course","Subject"};
    DefaultTableModel model = new DefaultTableModel(data,col);
    table = new JTable(model);
    ChangeName(table,0,"Stu_name");
    ChangeName(table,2,"Paper");
    JTableHeader header = table.getTableHeader();
    header.setBackground(Color.yellow);
    JScrollPane pane = new JScrollPane(table);
    panel.add(pane);
    frame.add(panel);
    frame.setUndecorated(true);
    frame.getRootPane().setWindowDecorationStyle(JRootPane.PLAIN_DIALOG);
    frame.setSize(500,150);
    frame.setVisible(true);
  }

  public void ChangeName(JTable table, int col_index, String col_name){
    table.getColumnModel().getColumn(col_index).setHeaderValue(col_name);
  }
}