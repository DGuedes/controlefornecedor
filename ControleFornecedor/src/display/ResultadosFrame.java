
package display;

import controlefornecedor.controller.ControleFornecedor;
import controlefornecedor.model.Fornecedor;
import controlefornecedor.model.PessoaFisica;
import controlefornecedor.model.PessoaJuridica;

/**
 *
 * @author dylan
 */
public class ResultadosFrame extends javax.swing.JFrame {

    ControleFornecedor umControleFornecedor;
    Fornecedor umFornecedor;
    PessoaJuridica umaPessoaJuridica;
    PessoaFisica umaPessoaFisica;
    
    public ResultadosFrame(PessoaJuridica umaPessoaJuridica, ControleFornecedor umControleFornecedor) {
        this.umControleFornecedor = umControleFornecedor;
        this.umFornecedor = umFornecedor;
        initComponents();
        jLabelNomeResultado.setText(umFornecedor.getNome());
    }
    public ResultadosFrame(PessoaFisica umaPessoaFisica, ControleFornecedor umControleFornecedor){
        this.umControleFornecedor = umControleFornecedor;
        this.umFornecedor = umFornecedor;
        initComponents();
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabelNomeResultado = new javax.swing.JLabel();
        jLabelCPFResultado = new javax.swing.JLabel();
        jButtonProdutoResultado = new javax.swing.JButton();
        jButtonTelefoneResultado = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jLabelRazaoSocial = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Nome:");

        jLabel2.setText("Endereço:");

        jLabel3.setText("CPF/CNPJ:");

        jLabel4.setText("Produtos:");

        jLabel5.setText("Telefones:");

        jLabelNomeResultado.setText("jLabel7");

        jLabelCPFResultado.setText("jLabel9");

        jButtonProdutoResultado.setText("jButton1");

        jButtonTelefoneResultado.setText("jButton2");

        jButton1.setText("jButton1");

        jLabel6.setText("Razão social:");

        jLabelRazaoSocial.setText("jLabel7");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel4)
                                .addComponent(jLabel3))
                            .addGap(35, 35, 35)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabelCPFResultado)
                                .addComponent(jButtonProdutoResultado)
                                .addComponent(jLabelNomeResultado)
                                .addComponent(jButton1)))
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel5)
                                .addComponent(jLabel6))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabelRazaoSocial)
                                .addComponent(jButtonTelefoneResultado)))))
                .addContainerGap(206, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabelNomeResultado))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jButton1))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabelCPFResultado))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jButtonProdutoResultado))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jButtonTelefoneResultado))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jLabelRazaoSocial))
                .addContainerGap(32, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButtonProdutoResultado;
    private javax.swing.JButton jButtonTelefoneResultado;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabelCPFResultado;
    private javax.swing.JLabel jLabelNomeResultado;
    private javax.swing.JLabel jLabelRazaoSocial;
    // End of variables declaration//GEN-END:variables
}
