package display;

import javax.swing.JFrame;
import controlefornecedor.controller.ControleFornecedor;
import controlefornecedor.model.PessoaFisica;
import controlefornecedor.model.PessoaJuridica;
import controlefornecedor.controller.ControleTelefone;
import controlefornecedor.controller.ControleProdutos;
import controlefornecedor.controller.ControleEndereco;
import display.EnderecoAdicionar;

public class AdicionarFornecedorFisico extends javax.swing.JFrame {
    
    ControleFornecedor umControleFornecedor;
    PessoaFisica umaPessoaFisica = new PessoaFisica();
    PessoaJuridica umaPessoaJuridica = new PessoaJuridica();
    ControleTelefone umControleTelefone = new ControleTelefone();
    ControleProdutos umControleProdutos = new ControleProdutos();
    ControleEndereco umControleEndereco = new ControleEndereco();
    boolean juridico;

    public void LimparTela(){
        jTextFieldAdicionarNome.setText("");
        jTextFieldAdicionarProduto.setText("");
        jTextFieldAdicionarTelefone.setText("");
        jTextFieldCPF.setText("");
    }
    
    public AdicionarFornecedorFisico(ControleFornecedor umControleFornecedor, boolean juridico) {
        initComponents();
        LimparTela();
        this.umControleFornecedor = umControleFornecedor;
        this.juridico = juridico;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jTextFieldAdicionarNome = new javax.swing.JTextField();
        jTextFieldAdicionarTelefone = new javax.swing.JTextField();
        jTextFieldAdicionarProduto = new javax.swing.JTextField();
        jButtonAdicionarTelefone = new javax.swing.JButton();
        jButtonAdicionarProduto = new javax.swing.JButton();
        jLabelHerançaOpçao1 = new javax.swing.JLabel();
        jTextFieldCPF = new javax.swing.JTextField();
        jButtonAdicionarEndereco = new javax.swing.JButton();
        jButtonSalvarPessoaFisica = new javax.swing.JButton();

        jLabel1.setText("Nome:");

        jLabel2.setText("Endereço:");

        jLabel3.setText("Telefones:");

        jLabel4.setText("Produtos:");

        jTextFieldAdicionarNome.setText("jTextField1");

        jTextFieldAdicionarTelefone.setText("jTextField3");

        jTextFieldAdicionarProduto.setText("jTextField4");

        jButtonAdicionarTelefone.setText("Adicionar Outro Telefone:");
        jButtonAdicionarTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdicionarTelefoneActionPerformed(evt);
            }
        });

        jButtonAdicionarProduto.setText("Adicionar Outro Produto:");
        jButtonAdicionarProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdicionarProdutoActionPerformed(evt);
            }
        });

        jLabelHerançaOpçao1.setText("CPF:");

        jTextFieldCPF.setText("jTextField1");

        jButtonAdicionarEndereco.setText("AdicionarEndereço");
        jButtonAdicionarEndereco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdicionarEnderecoActionPerformed(evt);
            }
        });

        jButtonSalvarPessoaFisica.setText("Salvar Pessoa");
        jButtonSalvarPessoaFisica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarPessoaFisicaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButtonSalvarPessoaFisica)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addGap(12, 12, 12)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButtonAdicionarEndereco, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                            .addComponent(jTextFieldAdicionarNome, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelHerançaOpçao1)
                            .addComponent(jLabel4)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldAdicionarProduto, javax.swing.GroupLayout.DEFAULT_SIZE, 168, Short.MAX_VALUE)
                            .addComponent(jTextFieldAdicionarTelefone, javax.swing.GroupLayout.DEFAULT_SIZE, 168, Short.MAX_VALUE)
                            .addComponent(jTextFieldCPF, javax.swing.GroupLayout.DEFAULT_SIZE, 168, Short.MAX_VALUE))))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButtonAdicionarProduto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonAdicionarTelefone, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextFieldAdicionarNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jButtonAdicionarEndereco))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTextFieldAdicionarTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonAdicionarTelefone))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jTextFieldAdicionarProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonAdicionarProduto))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldCPF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelHerançaOpçao1))
                .addGap(18, 18, 18)
                .addComponent(jButtonSalvarPessoaFisica)
                .addContainerGap(22, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void jButtonAdicionarEnderecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarEnderecoActionPerformed
    EnderecoAdicionar frame = new EnderecoAdicionar(umControleEndereco);     
    frame.setVisible(true);
    
}//GEN-LAST:event_jButtonAdicionarEnderecoActionPerformed

private void jButtonAdicionarTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarTelefoneActionPerformed
    umControleTelefone.AdicionarTelefone(jTextFieldAdicionarTelefone.getText());
    jTextFieldAdicionarTelefone.setText("");
}//GEN-LAST:event_jButtonAdicionarTelefoneActionPerformed

private void jButtonSalvarPessoaFisicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarPessoaFisicaActionPerformed
    umaPessoaFisica.setNome(jTextFieldAdicionarNome.getText());
    umaPessoaFisica.setCpf(jTextFieldCPF.getText());
    umaPessoaFisica.setEndereco(umControleEndereco.RetornarEndereco());
    umaPessoaFisica.setTelefones(umControleTelefone.RetornarLista());
    umaPessoaFisica.setProdutos(umControleProdutos.RetornarLista());
    umaPessoaFisica.setJuridico(juridico);
    umControleFornecedor.adicionarPessoaFisica(umaPessoaFisica);
    System.out.println("Pessoa Fisica Adicionada!");
    setVisible(false);    
}//GEN-LAST:event_jButtonSalvarPessoaFisicaActionPerformed

private void jButtonAdicionarProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarProdutoActionPerformed
    umControleProdutos.AdicionarProduto(jTextFieldAdicionarProduto.getText());
    jTextFieldAdicionarProduto.setText("");
}//GEN-LAST:event_jButtonAdicionarProdutoActionPerformed

    /**
     * @param args the command line arguments
     */
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdicionarEndereco;
    private javax.swing.JButton jButtonAdicionarProduto;
    private javax.swing.JButton jButtonAdicionarTelefone;
    private javax.swing.JButton jButtonSalvarPessoaFisica;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabelHerançaOpçao1;
    private javax.swing.JTextField jTextFieldAdicionarNome;
    private javax.swing.JTextField jTextFieldAdicionarProduto;
    private javax.swing.JTextField jTextFieldAdicionarTelefone;
    private javax.swing.JTextField jTextFieldCPF;
    // End of variables declaration//GEN-END:variables
}
